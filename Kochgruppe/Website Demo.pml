<?xml version="1.0" encoding="UTF-8" ?>
<Package name="Website Demo" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="index" src="html/de/index.html" />
        <File name="index" src="html/en/index.html" />
        <File name="index" src="html/index.html" />
        <File name="robotutils" src="html/robotutils.js" />
        <File name="touch" src="html/touch.js" />
        <File name="README" src="README.md" />
        <File name="jquery.min" src="html/jquery.min.js" />
        <File name="IMG_9059" src="html/de/IMG_9059.jpg" />
        <File name="Kochgruppe" src=".idea/Kochgruppe.iml" />
        <File name="encodings" src=".idea/encodings.xml" />
        <File name="misc" src=".idea/misc.xml" />
        <File name="modules" src=".idea/modules.xml" />
        <File name="vcs" src=".idea/vcs.xml" />
        <File name="workspace" src=".idea/workspace.xml" />
        <File name="512px-Christmas_Cookies_Plateful" src="html/de/512px-Christmas_Cookies_Plateful.JPG" />
        <File name="design" src="html/de/design.css" />
        <File name="weihnachtsmusik" src="html/de/weihnachtsmusik.html" />
        <File name="weihnachtsplaetzchen1" src="html/de/weihnachtsplaetzchen1.html" />
        <File name="weihnachtsplaetzchen10" src="html/de/weihnachtsplaetzchen10.html" />
        <File name="weihnachtsplaetzchen11" src="html/de/weihnachtsplaetzchen11.html" />
        <File name="weihnachtsplaetzchen12" src="html/de/weihnachtsplaetzchen12.html" />
        <File name="weihnachtsplaetzchen13" src="html/de/weihnachtsplaetzchen13.html" />
        <File name="weihnachtsplaetzchen14" src="html/de/weihnachtsplaetzchen14.html" />
        <File name="weihnachtsplaetzchen15" src="html/de/weihnachtsplaetzchen15.html" />
        <File name="weihnachtsplaetzchen2" src="html/de/weihnachtsplaetzchen2.html" />
        <File name="weihnachtsplaetzchen3" src="html/de/weihnachtsplaetzchen3.html" />
        <File name="weihnachtsplaetzchen4" src="html/de/weihnachtsplaetzchen4.html" />
        <File name="weihnachtsplaetzchen5" src="html/de/weihnachtsplaetzchen5.html" />
        <File name="weihnachtsplaetzchen6" src="html/de/weihnachtsplaetzchen6.html" />
        <File name="weihnachtsplaetzchen7" src="html/de/weihnachtsplaetzchen7.html" />
        <File name="weihnachtsplaetzchen8" src="html/de/weihnachtsplaetzchen8.html" />
        <File name="weihnachtsplaetzchen9" src="html/de/weihnachtsplaetzchen9.html" />
        <File name="weihnachtsplaetzchenEinleitung" src="html/de/weihnachtsplaetzchenEinleitung.html" />
    </Resources>
    <Topics />
    <IgnoredPaths />
</Package>
